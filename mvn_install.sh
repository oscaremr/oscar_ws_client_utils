#!/bin/sh

VERSION=2015.01.16
PROJECT_NAME=oscar_ws_client_utils

INSTALL_PATH_PREFIX=~/.m2/repository/org/oscarehr/${PROJECT_NAME}/${VERSION}/${PROJECT_NAME}-${VERSION}
mvn clean install
mvn install:install-file -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=../oscar_maven_repo/local_repo -DlocalRepositoryId=local_repo -DgroupId=org.oscarehr -DartifactId=${PROJECT_NAME} -Dversion=${VERSION} -Dfile=${INSTALL_PATH_PREFIX}.jar -DpomFile=${INSTALL_PATH_PREFIX}.pom
