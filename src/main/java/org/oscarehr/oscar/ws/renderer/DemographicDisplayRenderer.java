/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.renderer;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.ws.DemographicTransfer;

public final class DemographicDisplayRenderer
{
	/**
	 * the assumption here is the order of precedence is displayName -> alias -> normal name. My assumption might be wrong though... 
	 */
	public static String getDisplayName(DemographicTransfer demographicTransfer, boolean includeHin)
	{
		StringBuilder sb = new StringBuilder();

		String displayName = StringUtils.trimToNull(demographicTransfer.getDisplayName());
		String alias = StringUtils.trimToNull(demographicTransfer.getAlias());
		if (displayName != null)
		{
			sb.append(displayName);
		}
		else if (alias != null)
		{
			sb.append(alias);
		}
		else
		{
			sb.append(StringUtils.trimToEmpty(demographicTransfer.getLastName()));

			String firstName = StringUtils.trimToNull(demographicTransfer.getFirstName());
			if (firstName != null)
			{
				if (sb.length() > 0) sb.append(", ");
				sb.append(firstName);
			}
		}

		if (includeHin)
		{
			sb.append(" <");
			sb.append(StringUtils.trimToEmpty(demographicTransfer.getHin()));
			sb.append('>');
		}

		return(sb.toString());
	}
}
