/**
 * Copyright (c) 2001-2014. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.utils;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.oscarehr.oscar.ws.manager.LoginManager;
import org.oscarehr.oscar.ws.manager.ProgramManager;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.LoginResultTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.oscarehr.ws.ProgramTransfer;

/**
 * This is an example of an OscarServerCredentials implementation. The main method shows 
 * example usage (and runs under eclipse if your eclipse is setup properly).
 * There is no particular reason as to why you can't use this class in production. 
 */
public final class OscarServerCredentialsImpl implements OscarServerCredentials
{
	private static final String SESSION_KEY = OscarServerCredentialsImpl.class.getName() + ".SESSION_KEY";

	private String oscarWsUrl;
	private Integer oscarSecurityId;
	private String oscarSecurityTokenKey;

	public OscarServerCredentialsImpl(String oscarWsUrl, LoginResultTransfer loginResultTransfer)
	{
		this(oscarWsUrl, loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey());
	}

	public OscarServerCredentialsImpl(String oscarWsUrl, Integer oscarSecurityId, String oscarSecurityTokenKey)
	{
		this.oscarWsUrl = oscarWsUrl;
		this.oscarSecurityId = oscarSecurityId;
		this.oscarSecurityTokenKey = oscarSecurityTokenKey;
	}

	@Override
	public String getServerBaseUrl()
	{
		return(oscarWsUrl);
	}

	@Override
	public Integer getLoggedInSecurityId()
	{
		return(oscarSecurityId);
	}

	@Override
	public String getLoggedInPersonSecurityToken()
	{
		return(oscarSecurityTokenKey);
	}

	/**
	 * This method will login and return credentials of the logged in person.
	 * @param session can be null 
	 */
	public static OscarServerCredentialsImpl loginAndStoreInSession(String oscarWsUrl, String oscarUserName, String oscarPassword, HttpSession session) throws NotAuthorisedException_Exception
	{
		LoginResultTransfer loginResultTransfer = LoginManager.login(oscarWsUrl, oscarUserName, oscarPassword);

		OscarServerCredentialsImpl credentials = new OscarServerCredentialsImpl(oscarWsUrl, loginResultTransfer);

		if (session != null) session.setAttribute(SESSION_KEY, credentials);

		return(credentials);
	}

	public static OscarServerCredentialsImpl getOscarServerCredentialsFromSession(HttpSession session)
	{
		return (OscarServerCredentialsImpl) (session.getAttribute(SESSION_KEY));
	}

	public static void main(String... argv) throws Exception
	{
		String serverUrl = "https://127.0.0.1:8081/oscar";
		String userName = "oscardoc";
		String password = "mac2002";

		// this is to allow self signed certificates
		MiscUtils.setJvmDefaultSSLSocketFactoryAllowAllCertificates();

		// this will log you in
		OscarServerCredentialsImpl credentials = loginAndStoreInSession(serverUrl, userName, password, null);

		// get a list of all programs in oscar and print to the screen
		List<ProgramTransfer> allPrograms = ProgramManager.getAllPrograms(credentials);
		for (ProgramTransfer p : allPrograms)
		{
			System.err.println("program : " + p.getId() + ", " + p.getName() + ", " + p.getType());
		}
	}
}
