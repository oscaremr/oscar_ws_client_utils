/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.CxfClientUtils;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.AllergyWs;
import org.oscarehr.ws.AllergyWsService;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.DemographicWsService;
import org.oscarehr.ws.DocumentWs;
import org.oscarehr.ws.DocumentWsService;
import org.oscarehr.ws.FacilityWs;
import org.oscarehr.ws.FacilityWsService;
import org.oscarehr.ws.LoginWs;
import org.oscarehr.ws.LoginWsService;
import org.oscarehr.ws.MeasurementWs;
import org.oscarehr.ws.MeasurementWsService;
import org.oscarehr.ws.PrescriptionWs;
import org.oscarehr.ws.PrescriptionWsService;
import org.oscarehr.ws.PreventionWs;
import org.oscarehr.ws.PreventionWsService;
import org.oscarehr.ws.ProgramWs;
import org.oscarehr.ws.ProgramWsService;
import org.oscarehr.ws.ProviderWs;
import org.oscarehr.ws.ProviderWsService;
import org.oscarehr.ws.ScheduleWs;
import org.oscarehr.ws.ScheduleWsService;
import org.oscarehr.ws.SystemInfoWs;
import org.oscarehr.ws.SystemInfoWsService;

public class OscarServerWebServicesManager
{
	private static Logger logger = MiscUtils.getLogger();
	private static final int CACHE_POOLS = 4;
	private static final int PORT_CACHE_SIZE = 2;
	private static final long PORT_CACHE_TIME = DateUtils.MILLIS_PER_MINUTE * 5;

	private static QueueCache<String, SystemInfoWs> systemInfoWsCache = new QueueCache<String, SystemInfoWs>("OscarServerWebServicesManager.systemInfoWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<String, LoginWs> loginWsCache = new QueueCache<String, LoginWs>("OscarServerWebServicesManager.loginWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, FacilityWs> facilityWsCache = new QueueCache<OscarServerCredentials, FacilityWs>("OscarServerWebServicesManager.facilityWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, ProgramWs> programWsCache = new QueueCache<OscarServerCredentials, ProgramWs>("OscarServerWebServicesManager.programWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, ProviderWs> providerWsCache = new QueueCache<OscarServerCredentials, ProviderWs>("OscarServerWebServicesManager.providerWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, DemographicWs> demographicWsCache = new QueueCache<OscarServerCredentials, DemographicWs>("OscarServerWebServicesManager.demographicWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, ScheduleWs> scheduleWsCache = new QueueCache<OscarServerCredentials, ScheduleWs>("OscarServerWebServicesManager.scheduleWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, AllergyWs> allergyWsCache = new QueueCache<OscarServerCredentials, AllergyWs>("OscarServerWebServicesManager.allergyWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, PreventionWs> preventionWsCache = new QueueCache<OscarServerCredentials, PreventionWs>("OscarServerWebServicesManager.preventionWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, MeasurementWs> measurementWsCache = new QueueCache<OscarServerCredentials, MeasurementWs>("OscarServerWebServicesManager.measurementWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, DocumentWs> documentWsCache = new QueueCache<OscarServerCredentials, DocumentWs>("OscarServerWebServicesManager.documentWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, PrescriptionWs> prescriptionWsCache = new QueueCache<OscarServerCredentials, PrescriptionWs>("OscarServerWebServicesManager.prescriptionWsCache", CACHE_POOLS, PORT_CACHE_SIZE, PORT_CACHE_TIME, null);

	static
	{
		CxfClientUtils.initSslFromConfig();
	}

	private static URL buildURL(String serverBaseUrl, String servicePoint)
	{
		String urlString = serverBaseUrl + "/ws/" + servicePoint + "?wsdl";

		logger.debug(urlString);

		try
		{
			return(new URL(urlString));
		}
		catch (MalformedURLException e)
		{
			logger.error("Invalid Url : " + urlString, e);
			return(null);
		}
	}

	public static SystemInfoWs getSystemPropertiesWs(String serverBaseUrl)
	{
		SystemInfoWs port = systemInfoWsCache.get(serverBaseUrl);

		if (port == null)
		{
			SystemInfoWsService service = new SystemInfoWsService(buildURL(serverBaseUrl, "SystemInfoService"));
			port = service.getSystemInfoWsPort();

			CxfClientUtils.configureClientConnection(port);

			systemInfoWsCache.put(serverBaseUrl, port);
		}

		return(port);
	}

	public static LoginWs getLoginWs(String serverBaseUrl)
	{
		LoginWs port = loginWsCache.get(serverBaseUrl);

		if (port == null)
		{
			LoginWsService service = new LoginWsService(buildURL(serverBaseUrl, "LoginService"));
			port = service.getLoginWsPort();

			CxfClientUtils.configureClientConnection(port);

			loginWsCache.put(serverBaseUrl, port);
		}

		return(port);
	}

	public static FacilityWs getFacilityWs(OscarServerCredentials credentials)
	{
		FacilityWs port = facilityWsCache.get(credentials);

		if (port == null)
		{
			FacilityWsService service = new FacilityWsService(buildURL(credentials.getServerBaseUrl(), "FacilityService"));
			port = service.getFacilityWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			facilityWsCache.put(credentials, port);
		}

		return(port);
	}

	public static ProgramWs getProgramWs(OscarServerCredentials credentials)
	{
		ProgramWs port = programWsCache.get(credentials);

		if (port == null)
		{
			ProgramWsService service = new ProgramWsService(buildURL(credentials.getServerBaseUrl(), "ProgramService"));
			port = service.getProgramWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			programWsCache.put(credentials, port);
		}

		return(port);
	}

	public static ProviderWs getProviderWs(OscarServerCredentials credentials)
	{
		ProviderWs port = providerWsCache.get(credentials);

		if (port == null)
		{
			ProviderWsService service = new ProviderWsService(buildURL(credentials.getServerBaseUrl(), "ProviderService"));
			port = service.getProviderWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			providerWsCache.put(credentials, port);
		}

		return(port);
	}

	public static DemographicWs getDemographicWs(OscarServerCredentials credentials)
	{
		DemographicWs port = demographicWsCache.get(credentials);

		if (port == null)
		{
			DemographicWsService service = new DemographicWsService(buildURL(credentials.getServerBaseUrl(), "DemographicService"));
			port = service.getDemographicWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			demographicWsCache.put(credentials, port);
		}

		return(port);
	}

	public static ScheduleWs getScheduleWs(OscarServerCredentials credentials)
	{
		ScheduleWs port = scheduleWsCache.get(credentials);

		if (port == null)
		{
			ScheduleWsService service = new ScheduleWsService(buildURL(credentials.getServerBaseUrl(), "ScheduleService"));
			port = service.getScheduleWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			scheduleWsCache.put(credentials, port);
		}

		return(port);
	}

	public static AllergyWs getAllergyWs(OscarServerCredentials credentials)
	{
		AllergyWs port = allergyWsCache.get(credentials);

		if (port == null)
		{
			AllergyWsService service = new AllergyWsService(buildURL(credentials.getServerBaseUrl(), "AllergyService"));
			port = service.getAllergyWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			allergyWsCache.put(credentials, port);
		}

		return(port);
	}

	public static PreventionWs getPreventionWs(OscarServerCredentials credentials)
	{
		PreventionWs port = preventionWsCache.get(credentials);

		if (port == null)
		{
			PreventionWsService service = new PreventionWsService(buildURL(credentials.getServerBaseUrl(), "PreventionService"));
			port = service.getPreventionWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			preventionWsCache.put(credentials, port);
		}

		return(port);
	}

	public static MeasurementWs getMeasurementWs(OscarServerCredentials credentials)
	{
		MeasurementWs port = measurementWsCache.get(credentials);

		if (port == null)
		{
			MeasurementWsService service = new MeasurementWsService(buildURL(credentials.getServerBaseUrl(), "MeasurementService"));
			port = service.getMeasurementWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			measurementWsCache.put(credentials, port);
		}

		return(port);
	}

	public static DocumentWs getDocumentWs(OscarServerCredentials credentials)
	{
		DocumentWs port = documentWsCache.get(credentials);

		if (port == null)
		{
			DocumentWsService service = new DocumentWsService(buildURL(credentials.getServerBaseUrl(), "DocumentService"));
			port = service.getDocumentWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			documentWsCache.put(credentials, port);
		}

		return(port);
	}

	public static PrescriptionWs getPrescriptionWs(OscarServerCredentials credentials)
	{
		PrescriptionWs port = prescriptionWsCache.get(credentials);

		if (port == null)
		{
			PrescriptionWsService service = new PrescriptionWsService(buildURL(credentials.getServerBaseUrl(), "PrescriptionService"));
			port = service.getPrescriptionWsPort();

			CxfClientUtils.configureClientConnection(port);
			CxfClientUtils.addWSS4JAuthentication(credentials.getLoggedInSecurityId(), credentials.getLoggedInPersonSecurityToken(), port);
			prescriptionWsCache.put(credentials, port);
		}

		return(port);
	}
}
