/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.Calendar;
import java.util.List;

import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.DemographicWs;
import org.oscarehr.ws.Gender;
import org.oscarehr.ws.PhrVerificationTransfer;

public final class DemographicManager
{
	public static DemographicTransfer getDemographic(OscarServerCredentials credentials, Integer demographicId)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		DemographicTransfer result = demographicWs.getDemographic(demographicId);

		return(result);
	}

	public static List<DemographicTransfer> searchDemographicName(OscarServerCredentials credentials, String searchString, int startIndex, int itemsToReturn)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		List<DemographicTransfer> results = demographicWs.searchDemographicByName(searchString, startIndex, itemsToReturn);

		return(results);
	}

	public static DemographicTransfer getDemographicByMyOscarUserName(OscarServerCredentials credentials, String myOscarUserName)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		DemographicTransfer result = demographicWs.getDemographicByMyOscarUserName(myOscarUserName);
		return(result);
	}

	public static PhrVerificationTransfer getLatestPhrVerificationByDemographic(OscarServerCredentials credentials, Integer demographicId)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		PhrVerificationTransfer result = demographicWs.getLatestPhrVerificationByDemographic(demographicId);
		return(result);
	}

	public static boolean isPhrVerifiedToSendMessages(OscarServerCredentials credentials, Integer demographicId)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		boolean result = demographicWs.isPhrVerifiedToSendMessages(demographicId);
		return(result);
	}

	public static boolean isPhrVerifiedToSendMedicalData(OscarServerCredentials credentials, Integer demographicId)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		boolean result = demographicWs.isPhrVerifiedToSendMedicalData(demographicId);
		return(result);
	}

	public static List<DemographicTransfer> searchDemographicsByAttributes(OscarServerCredentials credentials, String hin, String firstName, String lastName, Gender gender, Calendar dateOfBirth, String city, String province, String phone, String email, String alias, int startIndex, int itemsToReturn)
	{
		DemographicWs demographicWs = OscarServerWebServicesManager.getDemographicWs(credentials);
		List<DemographicTransfer> results = demographicWs.searchDemographicsByAttributes(hin, firstName, lastName, gender, dateOfBirth, city, province, phone, email, alias, startIndex, itemsToReturn);
		return(results);
	}
}
