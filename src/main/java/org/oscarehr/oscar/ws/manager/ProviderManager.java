/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.List;

import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.ProviderPropertyTransfer;
import org.oscarehr.ws.ProviderTransfer;
import org.oscarehr.ws.ProviderWs;

public final class ProviderManager
{
	private static final int CACHE_POOLS = 4;
	private static final int CACHE_SIZE = 64;
	private static final long CACHE_TIME = DateUtils.MILLIS_PER_HOUR;

	private static QueueCache<OscarServerCredentials, List<ProviderTransfer>> providerCache = new QueueCache<OscarServerCredentials, List<ProviderTransfer>>("ProviderManager.providerCache", CACHE_POOLS, CACHE_SIZE, CACHE_TIME, null);
	private static QueueCache<String, List<ProviderPropertyTransfer>> providerPropertyCache = new QueueCache<String, List<ProviderPropertyTransfer>>("ProviderManager.providerPropertyCache", CACHE_POOLS, CACHE_SIZE, CACHE_TIME, null);

	public static List<ProviderTransfer> getProviders(OscarServerCredentials credentials)
	{
		List<ProviderTransfer> results = providerCache.get(credentials);

		if (results == null)
		{
			ProviderWs providerWs = OscarServerWebServicesManager.getProviderWs(credentials);
			try
			{
				results = providerWs.getProviders2(null);
			}
			catch (WebServiceException e)
			{ //Fall back to the older call.
				results = providerWs.getProviders(true);
			}
			providerCache.put(credentials, results);
		}

		return(results);
	}

	public static ProviderTransfer getProvider(OscarServerCredentials credentials, String providerNo)
	{
		List<ProviderTransfer> providers = getProviders(credentials);
		for (ProviderTransfer provider : providers)
		{
			if (provider.getProviderNo().equals(providerNo)) return(provider);
		}

		return(null);
	}

	private static String makeProviderPropertyCacheKey(OscarServerCredentials credentials, String providerNo, String propertyName)
	{
		return(credentials.getServerBaseUrl() + '|' + credentials.getLoggedInSecurityId() + '|' + providerNo + '|' + propertyName);
	}

	/**
	 * Both providerNo and propertyName are required
	 */
	public static List<ProviderPropertyTransfer> getProviderProperties(OscarServerCredentials credentials, String providerNo, String propertyName)
	{
		String cacheKey = makeProviderPropertyCacheKey(credentials, providerNo, propertyName);
		List<ProviderPropertyTransfer> results = providerPropertyCache.get(cacheKey);

		if (results == null)
		{
			ProviderWs providerWs = OscarServerWebServicesManager.getProviderWs(credentials);
			results = providerWs.getProviderProperties(providerNo, propertyName);
			providerPropertyCache.put(cacheKey, results);
		}

		return(results);
	}

	/**
	 * Assumes the property is a single entry property, and will return either null or the first item in the list.
	 * Both providerNo and propertyName are required
	 */
	public static ProviderPropertyTransfer getProviderProperty(OscarServerCredentials credentials, String providerNo, String propertyName)
	{
		List<ProviderPropertyTransfer> results = getProviderProperties(credentials, providerNo, propertyName);
		if (results.size() > 0) return(results.get(0));
		else return(null);
	}
}
