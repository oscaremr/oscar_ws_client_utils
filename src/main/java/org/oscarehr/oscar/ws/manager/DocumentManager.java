/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.Calendar;
import java.util.List;

import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.ws.DocumentTransfer;
import org.oscarehr.ws.DocumentWs;

public final class DocumentManager
{
	public static List<DocumentTransfer> getDocumentsUpdateAfterDate(OscarServerCredentials credentials, Calendar updatedAfterThisDateInclusive, int itemsToReturn)
	{
		DocumentWs documentWs = OscarServerWebServicesManager.getDocumentWs(credentials);
		List<DocumentTransfer> results = documentWs.getDocumentsUpdateAfterDate(updatedAfterThisDateInclusive, itemsToReturn);
		return(results);
	}

	public static DocumentTransfer getDocument(OscarServerCredentials credentials, Integer documentId)
	{
		DocumentWs documentWs = OscarServerWebServicesManager.getDocumentWs(credentials);
		DocumentTransfer result = documentWs.getDocument(documentId);
		return(result);
	}
}
