/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.FacilityTransfer;
import org.oscarehr.ws.FacilityWs;

public final class FacilityManager
{
	private static final int CACHE_POOLS = 4;
	private static final int CACHE_SIZE = 64;
	private static final long CACHE_TIME = DateUtils.MILLIS_PER_HOUR;

	private static QueueCache<OscarServerCredentials, List<FacilityTransfer>> facilityCache = new QueueCache<OscarServerCredentials, List<FacilityTransfer>>("FacilityManager.facilityCache", CACHE_POOLS, CACHE_SIZE, CACHE_TIME, null);

	public static List<FacilityTransfer> getFacilities(OscarServerCredentials credentials, Boolean active)
	{
		List<FacilityTransfer> results = facilityCache.get(credentials);

		if (results == null)
		{
			FacilityWs facilityWs = OscarServerWebServicesManager.getFacilityWs(credentials);
			results = facilityWs.getAllFacilities(active);
			facilityCache.put(credentials, results);
		}

		return(results);
	}

	public static FacilityTransfer getFacility(OscarServerCredentials credentials, Integer facilityId)
	{
		List<FacilityTransfer> facilities = getFacilities(credentials, null);
		for (FacilityTransfer facility : facilities)
		{
			if (facility.getId().equals(facilityId)) return(facility);
		}

		return(null);
	}
}
