/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.Calendar;
import java.util.List;

import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.ws.PreventionExtTransfer;
import org.oscarehr.ws.PreventionTransfer;
import org.oscarehr.ws.PreventionWs;

public final class PreventionManager
{
	public static List<PreventionTransfer> getUpdatedAfterDate(OscarServerCredentials credentials, Calendar updatedAfterThisDateInclusive, int itemsToReturn)
	{
		PreventionWs preventionWs = OscarServerWebServicesManager.getPreventionWs(credentials);
		List<PreventionTransfer> results = preventionWs.getPreventionsUpdatedAfterDate(updatedAfterThisDateInclusive, itemsToReturn);
		return(results);
	}

	public static PreventionTransfer getPrevention(OscarServerCredentials credentials, Integer preventionId)
	{
		PreventionWs preventionWs = OscarServerWebServicesManager.getPreventionWs(credentials);
		PreventionTransfer result = preventionWs.getPrevention(preventionId);
		return(result);
	}

	/**
	 * return the ext entry or null if none exists with that key
	 */
	public static PreventionExtTransfer getExtEntryByKey(PreventionTransfer preventionTransfer, String extKey)
	{
		for (PreventionExtTransfer preventionExtTransfer : preventionTransfer.getPreventionExts())
		{
			if (extKey.equals(preventionExtTransfer.getKey()))
			{
				return(preventionExtTransfer);
			}
		}

		return(null);
	}
}
