/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.Calendar;
import java.util.List;

import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.MeasurementMapTransfer;
import org.oscarehr.ws.MeasurementTransfer;
import org.oscarehr.ws.MeasurementWs;

public final class MeasurementManager
{
	private static QueueCache<OscarServerCredentials, List<MeasurementMapTransfer>> measurementsMapCache = new QueueCache<OscarServerCredentials, List<MeasurementMapTransfer>>("MeasurementManager.measurementsMapCache", 4, 100, null);

	public static List<MeasurementTransfer> getCreatedAfterDate(OscarServerCredentials credentials, Calendar updatedAfterThisDateInclusive, int itemsToReturn)
	{
		MeasurementWs measurementWs = OscarServerWebServicesManager.getMeasurementWs(credentials);
		List<MeasurementTransfer> results = measurementWs.getMeasurementsCreatedAfterDate(updatedAfterThisDateInclusive, itemsToReturn);
		return(results);
	}

	public static MeasurementTransfer getMeasurement(OscarServerCredentials credentials, Integer measurementId)
	{
		MeasurementWs measurementWs = OscarServerWebServicesManager.getMeasurementWs(credentials);
		MeasurementTransfer result = measurementWs.getMeasurement(measurementId);
		return(result);
	}

	/**
	 * @return the ID of the measurement just added.
	 */
	public static Integer addMeasurement(OscarServerCredentials credentials, MeasurementTransfer measurementTransfer)
	{
		MeasurementWs measurementWs = OscarServerWebServicesManager.getMeasurementWs(credentials);
		Integer resultId = measurementWs.addMeasurement(measurementTransfer);
		return(resultId);
	}

	public static List<MeasurementMapTransfer> getMeasurementMaps(OscarServerCredentials credentials)
	{
		List<MeasurementMapTransfer> results = measurementsMapCache.get(credentials);

		if (results == null)
		{
			MeasurementWs measurementWs = OscarServerWebServicesManager.getMeasurementWs(credentials);
			results = measurementWs.getMeasurementMaps();
			measurementsMapCache.put(credentials, results);
		}

		return(results);
	}

	public static MeasurementMapTransfer getMeasurementMapTransferByIdentCode(OscarServerCredentials credentials, String identCode)
	{
		for (MeasurementMapTransfer measurementMap : getMeasurementMaps(credentials))
		{
			if (identCode.equals(measurementMap.getIdentCode()))
			{
				return(measurementMap);
			}
		}

		return(null);
	}
}
