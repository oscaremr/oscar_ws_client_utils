/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.oscar.ws.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.oscar.ws.utils.OscarServerWebServicesManager;
import org.oscarehr.util.QueueCache;
import org.oscarehr.ws.ProgramProviderTransfer;
import org.oscarehr.ws.ProgramTransfer;
import org.oscarehr.ws.ProgramWs;

public final class ProgramManager
{
	private static final int CACHE_POOLS = 4;
	private static final int CACHE_SIZE = 64;
	private static final long CACHE_TIME = DateUtils.MILLIS_PER_HOUR;

	private static QueueCache<OscarServerCredentials, List<ProgramTransfer>> programCache = new QueueCache<OscarServerCredentials, List<ProgramTransfer>>("ProgramManager.programCache", CACHE_POOLS, CACHE_SIZE, CACHE_TIME, null);
	private static QueueCache<OscarServerCredentials, List<ProgramProviderTransfer>> programProvidersCache = new QueueCache<OscarServerCredentials, List<ProgramProviderTransfer>>("ProgramManager.programProvidersCache", CACHE_POOLS, CACHE_SIZE, CACHE_TIME, null);

	public static List<ProgramTransfer> getAllPrograms(OscarServerCredentials credentials)
	{
		List<ProgramTransfer> results = programCache.get(credentials);

		if (results == null)
		{
			ProgramWs programWs = OscarServerWebServicesManager.getProgramWs(credentials);
			results = programWs.getAllPrograms();
			programCache.put(credentials, results);
		}

		return(results);
	}

	public static List<ProgramTransfer> getProgramsInFacility(OscarServerCredentials credentials, Integer facilityId)
	{
		ArrayList<ProgramTransfer> results = new ArrayList<ProgramTransfer>();

		List<ProgramTransfer> programs = ProgramManager.getAllPrograms(credentials);
		int facilityIdInt = facilityId.intValue();
		for (ProgramTransfer program : programs)
		{
			if (facilityIdInt == program.getFacilityId()) results.add(program);
		}

		return(results);
	}

	public static ProgramTransfer getProgram(OscarServerCredentials credentials, Integer programId)
	{
		List<ProgramTransfer> programs = getAllPrograms(credentials);
		for (ProgramTransfer program : programs)
		{
			if (program.getId().equals(programId)) return(program);
		}

		return(null);
	}

	public static List<ProgramProviderTransfer> getAllProgramProviders(OscarServerCredentials credentials)
	{
		List<ProgramProviderTransfer> results = programProvidersCache.get(credentials);

		if (results == null)
		{
			ProgramWs programWs = OscarServerWebServicesManager.getProgramWs(credentials);
			results = programWs.getAllProgramProviders();
			programProvidersCache.put(credentials, results);
		}

		return(results);
	}

	public static List<ProgramProviderTransfer> getProgramProviders(OscarServerCredentials credentials, Integer programId)
	{
		ArrayList<ProgramProviderTransfer> results = new ArrayList<ProgramProviderTransfer>();

		List<ProgramProviderTransfer> programProviders = getAllProgramProviders(credentials);
		for (ProgramProviderTransfer programProvider : programProviders)
		{
			if (programProvider.getProgramId().equals(programId)) results.add(programProvider);
		}

		return(results);
	}
}
